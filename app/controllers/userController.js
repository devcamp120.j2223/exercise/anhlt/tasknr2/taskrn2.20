const userModel = require("../model/userModel");
const orderModel = require("../model/orderModel");

const mongoose = require("mongoose");

const createUser = (req, res) => {
    let body = req.body;
    if (!body.fullName) {
        res.status(400).json({
            message: `fullName is require`
        })
    } else if (!body.email) {
        res.status(400).json({
            message: `email is require`
        })
    } else if (!body.address) {
        res.status(400).json({
            message: `address is require`
        })
    } else if (!body.phone) {
        res.status(400).json({
            message: `phone is require`
        })
    } else {
        let user = {
            _id: mongoose.Types.ObjectId(),
            fullName: body.fullName,
            email: body.email,
            address: body.address,
            phone: body.phone
        }
        userModel.create(user, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Create User Success",
                    data: data
                })
            }
        })
    }
};

const getAllUser = (req, res) => {
    userModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All User Success",
                data: data
            })
        }
    })
};

const getAllLimitUser = (req, res) => {
    let limituser = req.query.limit;
    
    userModel.find().limit(limituser).exec((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Limit User Success",
                data: data
            })
        }
    })
};

const getAllSkipUser = (req, res) => {
    let skipuser = req.query.skip;
    
    userModel.find().skip(skipuser).exec((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Skip User Success",
                data: data
            })
        }
    })
};

const getAllSkipLimitUser = (req, res) => {
    let skipuser = req.query.skip;
    let limituser = req.query.limit;
    
    userModel.find().skip(skipuser).limit(limituser).exec((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Skip & Limit User Success",
                data: data
            })
        }
    })
};

const getAllSortSkipLimitUser = (req, res) => {
    let skipuser = req.query.skip;
    let limituser = req.query.limit;
    let sortfullname = req.query.sort;
    
    userModel.find().sort({fullName: sortfullname}).skip(skipuser).limit(limituser).exec((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Sort & Skip & Limit User Success",
                data: data
            })
        }
    })
};

const getAllSortUser = (req, res) => {
    let sortfullname = req.query.sort;
    
    userModel.find().sort({fullName: sortfullname}).exec((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Sort User Success",
                data: data
            })
        }
    })
};

const getUserById = (req, res) => {
    let id = req.params.userid;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `User id is invalid`
        })
    } else {
        userModel.findById(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Get User by Id Success",
                    data: data
                })
            }
        })
    }
};

const updateUserById = (req, res) => {
    let id = req.params.userid;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `User id is invalid`
        })
    } else if (!body.fullName) {
        res.status(400).json({
            message: `fullName is require`
        })
    } else if (!body.email) {
        res.status(400).json({
            message: `email is require`
        })
    } else if (!body.address) {
        res.status(400).json({
            message: `address is require`
        })
    } else if (!body.phone) {
        res.status(400).json({
            message: `phone is require`
        })
    } else {
        let user = {
            
            fullName: body.fullName,
            email: body.email,
            address: body.address,
            phone: body.phone
        }
        userModel.findByIdAndUpdate(id, user, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Update User by Id Success",
                    data: data
                })
            }
        })
    }
};

const deleteUserById = (req, res) => {
    let id = req.params.userid;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `User id is invalid`
        })
    }else{
        userModel.findByIdAndDelete(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(204).json({
                    status: "Delete User by Id Success",
                })
            }
        })
    }
}

module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllLimitUser, getAllSkipUser, getAllSortUser, getAllSkipLimitUser, getAllSortSkipLimitUser};