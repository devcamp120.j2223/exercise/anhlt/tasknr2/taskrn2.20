const userModel = require("../model/userModel");
const orderModel = require("../model/orderModel");


const mongoose = require("mongoose");

const createOrder = (req, res) => {
    let body = req.body;
    if (!body.pizzaSize) {
        res.status(400).json({
            message: `pizzaSize is require`
        })
    } else if (!body.pizzaType) {
        res.status(400).json({
            message: `pizzaType is require`
        })
    } else if (!body.status) {
        res.status(400).json({
            message: `status is require`
        })
    } else if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        res.status(400).json({
            message: `voucher id is invalid`
        })
    }else if (!mongoose.Types.ObjectId.isValid(body.drink)) {
        res.status(400).json({
            message: `drink id is invalid`
        })
    }
    else {
        let order = {
            _id: mongoose.Types.ObjectId(),
            orderCode: body.orderCode,
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            voucher: body.voucher,
            drink: body.drink,
            status: body.status,
            ngayTao: body.ngayTao,
            ngayCapNhat: body.ngayCapNhat,
        }
        orderModel.create(order, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Create order Success",
                    data: data
                })
            }
        })
    }
};

const getAllOrder = (req, res) => {
    orderModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Get All Order Success",
                data: data
            })
        }
    })
};

const getOrderById = (req, res) => {
    let id = req.params.orderid;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `order id is invalid`
        })
    } else {
        orderModel.findById(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Get order by Id Success",
                    data: data
                })
            }
        })
    }
};

const updateOrderById = (req, res) => {
    let id = req.params.orderid;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `order id is invalid`
        })
    } else if (!body.pizzaType) {
        res.status(400).json({
            message: `pizzaType is require`
        })
    } else if (!body.status) {
        res.status(400).json({
            message: `status is require`
        })
    } else if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        res.status(400).json({
            message: `voucher id is invalid`
        })
    }else if (!mongoose.Types.ObjectId.isValid(body.drink)) {
        res.status(400).json({
            message: `drink id is invalid`
        })
    }else {
        let order = {
            
            orderCode: body.orderCode,
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            voucher: body.voucher,
            drink: body.drink,
            status: body.status,
            ngayTao: body.ngayTao,
            ngayCapNhat: body.ngayCapNhat,
        }
        orderModel.findByIdAndUpdate(id, order, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    status: "Update Order by Id Success",
                    data: data
                })
            }
        })
    }
};

const deleteOrderById = (req, res) => {
    let id = req.params.orderid;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `User id is invalid`
        })
    }else{
        orderModel.findByIdAndDelete(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(204).json({
                    status: "Delete Order by Id Success",
                })
            }
        })
    }
}

module.exports = { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById};