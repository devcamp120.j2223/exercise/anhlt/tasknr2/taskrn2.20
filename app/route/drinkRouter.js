const express = require("express");
const router = express.Router();

router.get("/drinks", (req, res) => {
    res.status(200).json({
        message: "get all drink"
    })
});

router.get("/drinks/:drinkid", (req, res) => {
    let id = req.params.drinkid;
    res.status(200).json({
        message: "get drink with id: " + id
    })
});

router.put("/drinks/:drinkid", (req, res) => {
    let id = req.params.drinkid;
    let body = req.body;
    res.status(200).json({
        id,...body
    })
});

router.post("/drinks", (req, res) => {
    let body = req.body;
    
    res.status(200).json({
        ...body
    })
});

router.delete("/drinks/:drinkid", (req, res) => {
    let id = req.params.drinkid;
    
    res.status(200).json({
        message: "delete drink id: " + id
    })
});

module.exports = router;