const express = require("express");
const router = express.Router();

router.get("/vouchers", (req, res) => {
    res.status(200).json({
        message: "get all voucher"
    })
});

router.get("/vouchers/:voucherid", (req, res) => {
    let id = req.params.voucherid;
    res.status(200).json({
        message: "get voucher with id: " + id
    })
});

router.put("/vouchers/:voucherid", (req, res) => {
    let id = req.params.voucherid;
    let body = req.body;
    res.status(200).json({
        id,...body
    })
});

router.post("/vouchers", (req, res) => {
    let body = req.body;
    
    res.status(200).json({
        ...body
    })
});

router.delete("/vouchers/:voucherid", (req, res) => {
    let id = req.params.voucherid;
    
    res.status(200).json({
        message: "delete voucher id: " + id
    })
});

module.exports = router;