const express = require("express");
const {createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById} = require("../controllers/orderController");
const router = express.Router();

router.get("/orders", getAllOrder);

router.get("/orders/:orderid", getOrderById);

router.put("/orders/:orderid", updateOrderById);

router.post("/orders", createOrder);

router.delete("/orders/:orderid", deleteOrderById);

module.exports = router;