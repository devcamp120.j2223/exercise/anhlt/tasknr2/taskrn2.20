const express = require("express");
const {createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllLimitUser, getAllSkipUser, getAllSortUser, getAllSkipLimitUser, getAllSortSkipLimitUser} = require("../controllers/userController");
const router = express.Router();

router.get("/users", getAllUser);
router.get("/limit-users", getAllLimitUser);
router.get("/skip-users", getAllSkipUser);
router.get("/sort-users", getAllSortUser);
router.get("/skip-limit-users", getAllSkipLimitUser);
router.get("/sort-skip-limit-users", getAllSortSkipLimitUser);

router.get("/users/:userid", getUserById);

router.put("/users/:userid", updateUserById);

router.post("/users", createUser);

router.delete("/users/:userid", deleteUserById);

module.exports = router;