const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const drinkSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        unique: true
    },
    donGia: {
        type: Number,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("drink", drinkSchema);