const express = require("express");
const mongoose = require("mongoose");
const Drinkrouter = require("./app/route/drinkRouter");
const drinkModel = require("./app/model/drinkModel");
const voucherModel = require("./app/model/voucherModel");
const orderModel = require("./app/model/orderModel");
const userModel = require("./app/model/userModel");
const Voucherrouter = require("./app/route/voucherRouter");
const Userrouter = require("./app/route/userRouter");
const Orderrouter = require("./app/route/orderRouter");
const app = express();
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
    if(err){
        throw err
    }
    console.log("connect mongodb successful");
})

app.use(express.json());
app.use(express.urlencoded({
    extended:true
}))

app.get("/", (req, res) => {
    res.status(200).json({
        message : "Hello world"
    })
})

app.use("/", Drinkrouter);
app.use("/", Voucherrouter);
app.use("/", Userrouter);
app.use("/", Orderrouter);


app.listen(port, () => {
    console.log(`app running at port ${port}`)
}) 